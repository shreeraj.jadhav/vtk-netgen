cmake_minimum_required(VERSION 3.8)
message(STATUS "Remote VTK module: NetGen meshing.")
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
include(RemoteKitDir)

find_package(Netgen QUIET REQUIRED)

set(classes
  vtkNetGenVolume
)

remote_kit_dir(kit_dir)

vtk_module_add_module(VTK::MeshingNetGen
  CLASSES ${classes}
)
target_link_libraries(MeshingNetGen
  PUBLIC
    nglib
)
target_include_directories(MeshingNetGen
  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${kit_dir}>
)
